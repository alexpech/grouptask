# Задания по вычислительной практике.

Печинский Алексей Антонович, 2 курс, 8 группа

## Проекты

* [Задание по алгоритмизации 1](https://bitbucket.org/alexpech/task5)
* [Задание по алгоритмизации 2](https://bitbucket.org/alexpech/task10)
* [Задание по алгоритмизации 3](https://bitbucket.org/alexpech/task11)
* [Групповое задание](https://bitbucket.org/alexpech/grouptask)
* [Групповое задание(herokuapp)](https://vich-2020-mmf-2-course.herokuapp.com/)

